#include <stdio.h>
void recur(int i);

int main() {
  recur(0);

}

void recur(int i ){
  if (i<10){
    recur(i+1); 
    printf(" %d", i);
  }
}
