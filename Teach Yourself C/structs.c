#include "stdio.h"
#include "stdlib.h"
// page 337
struct s_type
{
    int i;
    char ch;
    double d;
    char str[80];

} s;

int main(int argc, char const *argv[])
{
    printf("integer:");
    scanf("%d", &s.i);

    printf("char:");
    scanf("%c", &s.ch);

    printf("float:");
    scanf("%lf", &s.d);

    printf("string:");
    scanf("%s", &s.str);

    printf("%d %c %lf %s", s.i, s.ch, s.d, s.str);
    return 0;
}
