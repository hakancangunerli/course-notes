#include "stdio.h"
#include "stdlib.h"
// page 298 
int main(int argc, char const *argv[])
{
    FILE *fp;
    char ch;

    if (argc != 3)
    {
        printf("use, find <filename> <ch>");
        exit(1);
    }
    // open file for input
    if ((fp = fopen(argv[1], "r")) == NULL)
    {
        printf("cannot open file");
        exit(1);
    }

    while ((ch == fgetc(fp)) != EOF)
    {
        if (ch == *argv[2])
        {
            printf("%c found", ch);
            break;
        }
        fclose(fp);
    }

    return 0;
}
